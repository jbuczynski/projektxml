import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class PrawdaCzyFalsz extends JFrame {
	
	public PrawdaCzyFalsz()
	{
		try {
	        // Set cross-platform Java L&F (also called "Metal")
	    UIManager.setLookAndFeel(
	    		"com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	       // handle exception
	    }
	    catch (ClassNotFoundException e) {
	       // handle exception
	    }
	    catch (InstantiationException e) {
	       // handle exception
	    }
	    catch (IllegalAccessException e) {
	       // handle exception
	    }
	
	setTitle("Prawda, czy te� Fa�sz?");
	// ustalenie rozk�adu - je�li trzeba, np:
	setLayout(new BorderLayout());

	// tworzenie komponent�w
	Container powZAwartosci = getContentPane();

	// Ustalenie domy�lnej operacji zamkni�cia okna
	setDefaultCloseOperation(EXIT_ON_CLOSE);

	// ustalenie rozmiar�w okna, np.:

	// ustalenie po�o�enia okna np. wycentrowanie
	setLocationRelativeTo(null);

	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
	Point newLocation = new Point(middle.x - (this.getWidth() / 2 + 400),
			middle.y - (this.getHeight() / 2 + 300));
	this.setLocation(newLocation);
	// pokazanie okna
	JPanel buttonPanel = new JPanel();
	buttonPanel.add(Prawda);
	buttonPanel.add(Falsz);
	JPanel mainPanel = new JPanel(new GridLayout(2, 1));
	mainPanel.add(pytanie);
	mainPanel.add(buttonPanel);
	
	this.add(mainPanel);

	JMenuBar pasekm = new JMenuBar();
	setJMenuBar(pasekm);
	JMenu menuPlik = new JMenu("Plik");
	pasekm.add(menuPlik);

	// do��cz do menu elementy Po��cz i Zamknij

	JMenuItem scores = new JMenuItem("Lista najlepszych");
	scores.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			
			
			
		}
	});
	menuPlik.add(scores);
	
	JMenuItem elemZamknij = new JMenuItem("Zamknij");
	elemZamknij.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent zdarzenie) {
			System.exit(0);
		}
	});
	menuPlik.add(elemZamknij);
	
	
	pack();
	setVisible(true);
	
	
	}
	
	
	private JButton Prawda = new JButton("Prawda");
	private JButton Falsz = new JButton("Fa�sz");
	private JLabel pytanie = new JLabel("TU BEDZIE PYTANIE");
	private pytanie[] pytania;
	private String Nick;
	private int currentIndex=0;
	private int correctAnswers;
	private pytanie currentQuestion;
	private Boolean odp;
	
	
	public static void main(String[] args) {

		PrawdaCzyFalsz pcf = new PrawdaCzyFalsz();
		pcf.init();
	
		pcf.play();
		
	}
	
	
	void play()
	{
		next();
		
	}
	
	 pytanie nextQuestion()
	{		
		pytanie q = pytania[currentIndex];	
		currentIndex++;
		return q;
	}
	 
	 void next()
	 {
		 if(currentIndex<pytania.length)
		 {
			 currentQuestion = nextQuestion();
			 pytanie.setText(currentQuestion.getQuestion());
			
			 
		}
		 else
		 {
			 	zapisz();
				JOptionPane.showMessageDialog(null, "To Koniec "+ Nick+". Twoj wynik to: " + String.valueOf(correctAnswers));
				int result = JOptionPane.showConfirmDialog(null, "Czy chcesz zagrac jescze raz?", null, JOptionPane.YES_NO_OPTION);
				System.out.println(result);
				if(result == 0) {
					shuffle(pytania);
					correctAnswers=0;
					currentIndex=0;
					play();
				} else {
					System.exit(0);
				}
				return;
			 
		 }
	 }
	 
	 
	
	 public static <T> void shuffle(T[] array) {
         for (int i = array.length; i > 1; i--) {
                 T temp = array[i - 1];
                 int randIx = (int) (Math.random() * i);
                 array[i - 1] = array[randIx];
                 array[randIx] = temp;
         }
 }
	
	void init()
	{
		
		wczytaj();
		
		Prawda.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				odp = true;
				sprawdz();
				next();
			}
		});
	
		
		
		Falsz.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				odp = false;
				sprawdz();
				next();
			}
		});
		
		/*wczytajNick w = new wczytajNick(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Nick = ((wczytajNick)(e.getSource())).getNick();
				System.out.print(Nick);
			}
		});*/
		wczytajNick w = new wczytajNick();
		Nick = w.getNick();
	}
	
	void sprawdz()
	{
		
		
		 Boolean correctAnswer= currentQuestion.getAnswer();
		 
		 if(odp == correctAnswer)
		 {
			 correctAnswers++;
		 }
	}
	
	void wczytaj()
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.parse("pytania.xml");
			NodeList nl = dom.getElementsByTagName("pytanie");
			pytania = new pytanie[nl.getLength()]; //tablica pytan
			

			if(nl != null && nl.getLength() > 0) {
				for(int i = 0 ; i < nl.getLength();i++) {

					pytania[i] = new pytanie();
					Element el = (Element)nl.item(i);
					pytania[i].setQuestion( el.getElementsByTagName("tresc").item(0).getTextContent() );
					pytania[i].setAnswer( Boolean.parseBoolean(el.getElementsByTagName("odp").item(0).getTextContent()) );
				
				}
			
			}
		
		
	}
     catch(Exception e) {
	      System.err.println("Error: " + e.getMessage());
	      e.printStackTrace();
	    }
		
		shuffle(pytania);
	}
	
	
	void zapisz()
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document dom = db.parse("wyniki.xml");
		    Node nd1 = dom.getDocumentElement();	
		    Node wynik = dom.createElementNS("", "wynik");
		    Node nick = dom.createElementNS("", "Nick");
		    Node punkty = dom.createElementNS("", "Punkty");
		  
		    Node wynikValue = dom.createTextNode( String.valueOf(correctAnswers));
		    Node NickValue = dom.createTextNode(Nick);
		    
		   // NodeList nl = dom.getElementsByTagName("punkty");
		    
		    //int aw = Integer.parseInt(this.wynik.getText());
		    
		  
		 
		    	nick.appendChild(NickValue);
		    	wynik.appendChild(nick);
		    	
		    	punkty.appendChild(wynikValue);
		    	wynik.appendChild(punkty);
		    	
		    	nd1.appendChild(wynik);
		 
		
		    OutputFormat format = new OutputFormat(dom);
		    format.setIndenting(true);
		    format.setIndent(3);
		    format.setLineWidth(65);             
		    XMLSerializer serializer = new XMLSerializer (
		        new FileOutputStream("wyniki.xml"), format);
		    serializer.asDOMSerializer();
		    serializer.serialize(dom);

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	}

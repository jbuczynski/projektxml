
public class pytanie {

	private String pytanie;
	private Boolean odpowiedz;
	
	
	public pytanie()
	{
		
	}
	
	public void setQuestion(String q)
	{
		pytanie = q;
	}
	
	public void setAnswer(Boolean q)
	{
		odpowiedz = q;
	}
	
	public Boolean getAnswer()
	{
		return odpowiedz;
	}
	
	public String getQuestion()
	{
		return pytanie;
	}
	
	
	
}

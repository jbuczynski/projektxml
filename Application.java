package pl.kkrampa;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.Timer;

import java.awt.Font;
import javax.swing.BoxLayout;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JLabel;

public class Application extends JFrame {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2517616123689799182L;
	
	private List<Pytanie> pytania = new ArrayList<Pytanie>();
	private Pytanie aktualnePytanie;
	private JTextPane txtpn;
	private JLabel wynik;
	private JLabel lblDobrze;
	private JButton btnNewButton_1;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Application() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1);
		
		JLabel lblWynik = new JLabel("Wynik:");
		panel_1.add(lblWynik);
		
		wynik = new JLabel("0");
		panel_1.add(wynik);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		
		txtpn = new JTextPane();
		txtpn.setFont(new Font("Dialog", Font.PLAIN, 18));
		txtpn.setEditable(false);
		panel.add(txtpn);
		
		JPanel panel_2 = new JPanel();
		panel_2.setAlignmentY(Component.TOP_ALIGNMENT);
		getContentPane().add(panel_2);
		
		btnNewButton_1 = new JButton("Prawda");
		panel_2.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(aktualnePytanie.getOdpowiedz() == true) {
					lblDobrze.setText("Dobrze");
					Integer liczba = Integer.parseInt(wynik.getText());
					liczba++;
					wynik.setText(liczba.toString());
				} else {
					lblDobrze.setText("Źle");
				}
				btnNewButton.setEnabled(false);
				btnNewButton_1.setEnabled(false);
				Timer timer = new Timer(1000, new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						
						nastepnePytanie();
					}
					
				});
				timer.setRepeats(false);
				timer.start();

			}
		});
		
		btnNewButton = new JButton("Fałsz");
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(aktualnePytanie.getOdpowiedz() == false) {
					System.out.println("Zgadłeś!");
					Integer liczba = Integer.parseInt(wynik.getText());
					liczba++;
					wynik.setText(liczba.toString());
					lblDobrze.setText("Dobrze");

				} else {
					lblDobrze.setText("Źle");
				}
				btnNewButton.setEnabled(false);
				btnNewButton_1.setEnabled(false);
				Timer timer = new Timer(1000, new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						nastepnePytanie();
					}
					
				});
				timer.setRepeats(false);
				timer.start();
			}
		});
		panel_2.add(btnNewButton);
		
		lblDobrze = new JLabel();
		panel_2.add(lblDobrze);
		startGame();
	
	}
	
	public void startGame() {
		wynik.setText("0");
		loadQuestions();
		nastepnePytanie();
	}
	
	public void nastepnePytanie() {
		lblDobrze.setText("");
		btnNewButton.setEnabled(true);
		btnNewButton_1.setEnabled(true);
		aktualnePytanie = losujPytanie();
		if(aktualnePytanie == null) {
			zapiszWynik();
			JOptionPane.showMessageDialog(null, "Koniec gry. Twój wynik to: " + wynik.getText());
			int result = JOptionPane.showConfirmDialog(null, "Czy chcesz zacząć od nowa?", null, JOptionPane.YES_NO_OPTION);
			System.out.println(result);
			if(result == 0) {
				startGame();
			} else {
				System.exit(0);
			}
			return;
		}
		txtpn.setText(aktualnePytanie.getTresc());
	}
	
	public Pytanie losujPytanie() {
		if(pytania.size() == 0)
			return null;
		return pytania.remove(0);
	}
	
	public void loadQuestions() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.parse("pytania.xml");
			NodeList nl = dom.getElementsByTagName("pytanie");

			if(nl != null && nl.getLength() > 0) {
				for(int i = 0 ; i < nl.getLength();i++) {

					//get the employee element
					Element el = (Element)nl.item(i);
					Pytanie pytanie = new Pytanie();
					String tresc = el.getElementsByTagName("tresc").item(0).getTextContent();
					pytanie.setTresc(tresc);
					String odpowiedz = el.getElementsByTagName("odp").item(0).getTextContent();
					pytanie.setOdpowiedz(odpowiedz.equalsIgnoreCase("True"));
					pytania.add(pytanie);
				}
				Collections.shuffle(pytania);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
	
	public void zapiszWynik() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document dom = db.parse("wyniki.xml");
		    Node nd1 = dom.getDocumentElement();	
		    Node nd2 = dom.createElementNS("", "wynik");
		    Node imie = dom.createElementNS("", "imie");
		    Node wynik = dom.createElementNS("", "punkty");
		    Node imieTxt = null;
		    Node wynikTxt = dom.createTextNode(this.wynik.getText());
		    NodeList nl = dom.getElementsByTagName("punkty");
		    int aw = Integer.parseInt(this.wynik.getText());
		    
		    Boolean inserted = false;
		    for (int i = 0; i < nl.getLength(); i++) {
		    	int w = Integer.parseInt(nl.item(i).getTextContent());
		    	if(aw > w) {
		    		nd1.insertBefore(nd2, nl.item(i).getParentNode());
		    		String txt = JOptionPane.showInputDialog("Podaj imię: ");
			    	imieTxt = dom.createTextNode(txt);
		    		inserted = true;
		    		break;
		    	}
		    }

		    if(!inserted && nl.getLength() < 5) {
		    	String txt = JOptionPane.showInputDialog("Podaj imię: ");
		    	imieTxt = dom.createTextNode(txt);
		    	nd1.appendChild(nd2);
		    	inserted = true;
		    }
		    
		    if(!inserted)
		    	return;
		    imie.appendChild(imieTxt);
		    wynik.appendChild(wynikTxt);
		    nd2.appendChild(imie);
		    nd2.appendChild(wynik);
		    

		    if(nl.getLength() > 5) 
		    	nl.item(5).getParentNode().getParentNode().removeChild(nl.item(5).getParentNode());
		    
		
		
		    OutputFormat format = new OutputFormat(dom);
		    format.setIndenting(true);
		    format.setIndent(3);
		    format.setLineWidth(65);             
		    XMLSerializer serializer = new XMLSerializer (
		        new FileOutputStream("wyniki.xml"), format);
		    serializer.asDOMSerializer();
		    serializer.serialize(dom);

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
